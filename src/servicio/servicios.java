/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.longitud;
import datos.peso;
import datos.temperatura;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Jadpa26
 */
public class servicios {
    public ComboBoxModel getCmbModelLongitud(){
        return new DefaultComboBoxModel(longitud.values());
    }   
    public ComboBoxModel getCmbModelpeso(){
        return new DefaultComboBoxModel(peso.values());
    } 
    public ComboBoxModel getCmbModeltemperatura(){
        return new DefaultComboBoxModel(temperatura.values());
    }     
}
